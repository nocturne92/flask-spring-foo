'''
Created on Dec 23, 2016

@author: nocturne
'''

class EmployeeSerializer():
    def __init__(self, model):
        self.model = model
    
    def serialize(self):
        return {'id': self.model.id, 'firstName': self.model.firstName, 'lastName': self.model.lastName}