'''
Created on Dec 23, 2016

@author: nocturne
'''
from springpython.config import PythonConfig, Object
from springpython.context import scope
from flaskspringfoo.dao.employee_dao import EmployeeDAO
from springpython.database.factory import Sqlite3ConnectionFactory
from springpython.database.core import DatabaseTemplate

class AppContext(PythonConfig):
    def __init__(self):
        super(AppContext, self).__init__()
        
    @Object(scope.SINGLETON)
    def databaseTemplate(self):
        connectionFactory = Sqlite3ConnectionFactory(db = "db.sqlite3")
        return DatabaseTemplate(connectionFactory)
        
    @Object(scope.SINGLETON)
    def employeeDao(self):
        return EmployeeDAO(dt = self.databaseTemplate())