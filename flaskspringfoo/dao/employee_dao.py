'''
Created on Dec 23, 2016

@author: nocturne
'''
from springpython.database.core import SimpleRowMapper
from flaskspringfoo.model import Employee
from flaskspringfoo.exceptions import RecordNotFound, RecordInvalid
from flaskspringfoo.validators.employee_validator import EmployeeValidator

class EmployeeDAO:
    def __init__(self, dt = None):
        self.dt = dt
    
    def listAll(self):
        return self.dt.query("select * from employees", rowhandler=SimpleRowMapper(Employee))
    
    def get(self, employeeId):
        try:
            return self.dt.query(
                "select * from employees where id=?",
                (employeeId,), rowhandler=SimpleRowMapper(Employee))[0]
        except IndexError as e:
            raise RecordNotFound(e)
    
    def create(self, employee):
        EmployeeValidator(employee).validateRaise()
        
        return self.dt.insert_and_return_id(
            "insert into employees (firstName, lastName) values (?, ?)",
            (employee.firstName, employee.lastName))
        
    def update(self, employee):
        EmployeeValidator(employee).validateRaise()
        
        return self.dt.update(
            "update employees set firstName = ?, lastName = ? where id = ?",
            (employee.firstName, employee.lastName, employee.id))
        
    def delete(self, employeeId):
        return self.dt.update(
            "delete from employees where id = ?",
            (employeeId,))