'''
Created on Dec 23, 2016

@author: nocturne
'''
from flaskspringfoo.exceptions import RecordInvalid

class EmployeeValidator():
    def __init__(self, employee):
        self.employee = employee
        
    def validate(self):
        errors = []
                
        if (not (self.employee.firstName and self.employee.firstName.strip())):
            errors.append({'firstName': 'May not be empty'})
            
        if (not (self.employee.lastName and self.employee.lastName.strip())):
            errors.append({'lastName': 'May not be empty'})
            
        return errors
    
    def validateRaise(self):
        errors = self.validate()
        if len(errors) > 0:
            raise RecordInvalid(record=self.employee, errors=errors)