'''
Created on Dec 23, 2016

@author: nocturne
'''
from flaskspringfoo.model.employee import Employee
from flaskspringfoo.serializers.employee import EmployeeSerializer
from flask.json import JSONEncoder

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, Employee):
                return EmployeeSerializer(obj).serialize()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)