'''
Created on Dec 23, 2016

@author: nocturne
'''

class RecordNotFound(Exception):
    pass

class RecordInvalid(Exception):
    status_code = 400
    
    def __init__(self, record=None, errors=[]):
        super(RecordInvalid, self).__init__()
        self.record = record
        self.errors = errors
        
    def to_dict(self):
        return {'errors': self.errors}