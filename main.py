'''
Created on Dec 23, 2016

@author: nocturne
'''

from flask import Flask, make_response, abort
from flaskspringfoo.app_context import AppContext
from flask_spring import Spring
from flask.templating import render_template
from flaskspringfoo.exceptions import RecordNotFound, RecordInvalid
from flask.json import jsonify
from flaskspringfoo.model import Employee
from flask.globals import request
from functools import wraps
from flaskspringfoo.custom_json_encoder import CustomJSONEncoder

app = Flask(__name__)
app.config['SPRING_OBJS'] = [AppContext()]
app.json_encoder = CustomJSONEncoder

@app.errorhandler(RecordInvalid)
def handle_record_invalid(e):
    response = jsonify(e.errors)
    response.status_code = 400
    return response

spring = Spring(app)

def with_context(f):
    @wraps(f)
    def wrap(*arg, **kwarg):
        return f(spring.context, *arg, **kwarg)
    return wrap

@app.route("/employees")
@with_context
def employee_index(ctx):
    employees = ctx.get_object("employeeDao").listAll()
    return render_template("employee/index.html", employees=employees)

@app.route("/employees/<int:id>")
@with_context
def employee_show(ctx, id):
    employee = ctx.get_object("employeeDao").get(id)
    return render_template("employee/show.html", employee=employee)

@app.route("/employees/new")
def employee_new():
    return render_template("employee/new.html")

@app.route("/employees/<int:id>.json", methods=['PUT'])
@with_context
def employee_update_json(ctx, id):
    employee = Employee(id, request.form['firstName'], request.form['lastName'])
    ctx.get_object("employeeDao").update(employee)
    return make_response(jsonify({}), 204)

@app.route("/employees.json", methods=['POST'])
@with_context
def employee_create(ctx):
    employee = Employee(firstName=request.form['firstName'], lastName=request.form['lastName'])
    employee.id = ctx.get_object("employeeDao").create(employee)
    return jsonify({'employee': employee})

@app.route("/employees/<int:id>/edit")
@with_context
def employee_edit(ctx, id):
    employee = ctx.get_object("employeeDao").get(id)
    return render_template("employee/edit.html", employee=employee)

@app.route("/employees/<int:id>.json", methods=['DELETE'])
@with_context
def employee_delete(ctx, id):
    ctx.get_object("employeeDao").delete(id)
    return make_response(jsonify({}), 204)

if __name__ == "__main__":
    app.run(debug=True)