$(document).ready(function() {
    $('a[data-ajax]').each(function() {
        var el = this;
        $(this).on('click', function(e) {
            e.preventDefault();
            
            if (typeof(el.dataset.confirm) !== 'undefined') {
                if (!window.confirm('Confirm')) {
                    return;
                }
            }
                            
            var method = el.dataset.method;
            method = method ? method : 'POST';
            
            $.ajax({
                url: el.href,
                method: method,
                success: function() {
                    if (typeof(el.dataset.refresh) !== 'undefined') {
                        window.location.reload();
                    }
                }
            });
        });
    });
});